# CHISE dump Character indexes
A JSON dump of indexes of character objects of the CHISE character ontology.


## Format

Each file is stored in the following path:

> index/*grain*/*CCS*/*domain*/*cpos*.json

while

> *grain* = = "a2" | "a" | "o" | "rep" | "g" | "g2" | "repi"

> *CCS* = *base name of CCS feature*

> *domain* = "default" | *domain-iri-rep*

> *cpos* = *decimal number to represent code point of CCS*

For example, index file of Japanese representative glyph of U+6F22
(rep.ucs@jis=0x6F22) is index/rep/ucs/jis/28450.json.

It is similar to the character object representation of CHISE-wiki / E<sub>S</sub>T.

Each JSON file has object location, such as:

> rep/adobe-japan1-0/default/0x0/05/fd
